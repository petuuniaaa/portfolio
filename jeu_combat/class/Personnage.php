<?php

class Personnage {
    const MAXLIFE = 100;
    private static $compteur = 0;
    protected $name;
    protected $pv;
    protected $atk;
    protected $id;
    protected $img;

    public function getPv(){
        return $this-> pv;
    }
    public function getAtk(){
        return $this-> atk;
    }
    public function getName(){
        return $this-> name;
    }

    public function getId(){
        return $this-> id;
    }

    public function getImg(){
        return $this-> img;
    }

    public function setPv($pv){
        if ($pv > 100) {
            $this-> pv = 100;
        } elseif ($pv < 0) {
            $this-> pv = 0;
        } else {
            $this-> pv = $pv;
        }
    }
    public function setAtk($atk){
        $this-> atk = $atk;
    }
    public function setName($name){
        $this-> name = $name;
    }

    public function setId($id){
        $this-> id = $id;
    }

    public function setImg($img){
        $this-> img = $img;
    }

    public function hydrate(array $donnees){
        foreach ($donnees as $key => $value){
            $method = 'set'.ucfirst($key);
            if (method_exists($this, $method)){        
                $this->$method($value);     
            }
        }           
    }

    public function __construct(array $donnees){
        $this-> hydrate($donnees);
        self::$compteur ++;
    }

    public static function afficheCompteur(){
        return self::$compteur;
    }

    public function crier(){
        return ($this->name . " dit : Vous ne passerez pas !");
    }

    public function regenerer($y){
        $this -> setPv($this -> pv + $y);
    }

    public function is_alive() {
        if ($this-> pv <= 0){
            return($this-> name . " est mort(e)");
        }
        else {
            return($this-> name . " est vivant(e)");
        }
    }

    public function attaque(Personnage $victime){
        $victime-> pv -= $this -> atk;
    }

    public function reinitPv(){
        $this-> pv  = self::MAXLIFE;
    }
}
?>