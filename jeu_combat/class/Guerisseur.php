<?php
class Guerisseur extends Personnage {
    private $heal = 10;

    public function regenerer(Personnage $perso){
        $perso -> pv += $this -> heal;
    }
}
?>