function site() {
    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    //Récupérer navigation principale
    var navigation = document.querySelector("nav");


    // element qui scrolle
    var scrollingElement = document.scrollingElement;

    var debouncedScroll = debounce(onscroll, 25);
    window.addEventListener("scroll", debouncedScroll);


    function onscroll() {
        if (scrollingElement.scrollTop === 0) {
            navigation.classList.contains("navcolor");
            navigation.classList.replace("navcolor", "bg-light");
            return;
        } else {
            navigation.classList.replace("bg-light", "navcolor");
            return;
        }

    }
};


document.addEventListener("DOMContentLoaded", site);