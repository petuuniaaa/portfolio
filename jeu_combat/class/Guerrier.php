<?php 
class Guerrier extends Personnage {
    const BONUS = 20;

    public function attaque(Personnage $victime){
        $victime -> pv -= $this -> atk + self::BONUS;
    }
}

?>