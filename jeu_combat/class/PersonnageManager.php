<?php

class PersonnageManager {
    public $db;

    public function __construct($db){
        $this -> setDb($db);
    }

    public function setDb(PDO $db){
        $this ->db = $db;
    }

    public function getLastPersonnage($id){
        $query = "SELECT * FROM personnages WHERE id ORDER BY id DESC";
        $result = $this->db->query($query);

        $donnees = $result->fetch(PDO::FETCH_ASSOC);
            $perso = new Personnage($donnees);
        return $perso;
    }

    public function getOnePersonnageById($id){
        $query = "SELECT * FROM personnages WHERE id = ?";
        $result = $this->db->prepare($query);
        $result->execute(array($id));

        $donnees = $result->fetch(PDO::FETCH_ASSOC);
            $perso = new Personnage($donnees);
        return $perso;
    }

    public function getAllPersonnage(){
        $query = "SELECT * FROM `personnages` ORDER BY `personnages`.`pv` DESC";
        $result = $this->db->query($query);

        while ($donnees = $result->fetch(PDO::FETCH_ASSOC)){
            $personnages[] = new Personnage($donnees);
        }
        return $personnages;
    }

    public function insertPersonnage(Personnage $personnages){
        $stmt = $this->db->prepare("INSERT INTO personnages (name, pv, atk) VALUES (:name, :pv, :atk)");
        $name=$personnages->getName();
        $pv=$personnages->getPv();
        $atk=$personnages->getAtk();
        $img=$personnages->getImg();

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':pv', $pv);
        $stmt->bindParam(':atk', $atk);
        // $stmt->bindParam(':img', $img);

        $stmt->execute();
    }

    public function deletePerso($persoID){
        $stmt = $this->db->prepare("DELETE FROM personnages WHERE id = ?");
        $stmt->execute(array($persoID));
    }

    public function updatePerso($caracter){
        $stmt = $this->db->prepare("UPDATE personnages SET pv = :pv WHERE id = :id");
        $stmt->execute(array('id'=> $caracter->getId(), 'pv'=> $caracter->getPv()));
    } 
}
?>