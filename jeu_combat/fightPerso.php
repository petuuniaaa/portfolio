<?php
require('init.php');

$manager = new PersonnageManager($db);

$personnages = $manager->getAllPersonnage();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="style.css">
    <title>Fight !</title>
</head>

<body>
    <?php

    if (isset($_POST["perso-select1"]) && $_POST["perso-select2"]) {
        $attaquant = $manager->getOnePersonnageById($_POST["perso-select1"]);
        $victime = $manager->getOnePersonnageById($_POST["perso-select2"]);

        if ($attaquant == $victime) {
            echo '<h1 class="erreur">Erreur : vous avez choisi le même dieu !</h1>';
        }
        // elseif(){
        //     echo '<h1 class="erreur">Erreur : vous avez choisi un dieu qui n\'a plu de pv !</h1>';
        // } 
        else {
            echo '<h1>Combat entre ' . $attaquant->getName() . ' et ' . $victime->getName() . '</h1>';
            echo '<div id="jeu" color="#FFF">';
            $attaquant->crier();
            echo '<br>';
            echo '<img  src="/jeu_combat/img/' . $attaquant->getImg() . '" alt=""/>';
            echo '<br>';
            echo $attaquant->getName() . " possède " . $attaquant->getPv() . " pv et attaque de  " . $attaquant->getAtk();
            echo '<br>';
            $attaquant->attaque($victime);
            echo $victime->is_alive() . " et possède plus que " . $victime->getPv() . "pv";
            $manager->updatePerso($victime);
            echo '<br>';

            echo '<br>';
            echo '<p>' . $victime->getName() . ' riposte</p>';
            echo '<br>';
            echo '<img  src=/jeu_combat/img/' . $victime->getImg() . ' alt=""/>';
            echo '<br>';
            echo $victime->getName() . " possède " . $victime->getPv() . " pv et attaque de  " . $victime->getAtk();
            echo '<br>';
            $victime->attaque($attaquant);
            echo $attaquant->is_alive() . " et possède plus que " . $attaquant->getPv() . "pv";
            $manager->updatePerso($attaquant);

            echo '<br>';
            if ($victime->getPv() > $attaquant->getPv()) {
                echo $victime->getName() . " a gagné le duel";
            } else {
                echo $attaquant->getName() . " a gagné le duel";
            }
            '</div>';
        }
    }
    ?>
    <a href="index.php"><button>Retour</button></a><br>
</body>

</html>