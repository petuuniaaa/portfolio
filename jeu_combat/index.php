<?php 
require('init.php');

$manager = new PersonnageManager($db);

$personnages = $manager->getAllPersonnage();

if(!empty($_POST['refresh'])) {
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Jeu de duel</title>
</head>

<body>
    <header>
        <h1>Bienvenue sur le jeu de duel de dieux Grecs</h1>
        <?php echo '<p>Il y a ' . Personnage::afficheCompteur() . ' personnages dans le jeu</p>' ; ?>
    </header>
    <div class="container">
    <p>Choississez 2 dieux pour le prochain combat :</p>
        <form action="fightPerso.php" method="POST">
            <select name="perso-select1">
                <?php 
        foreach ($personnages as $perso) {
            echo '<option value="'.$perso->getId().'">'.$perso->getName().'</option>';
        }
        ?>
            </select>
            <strong>Versus</strong>
            <select name="perso-select2">
                <?php 
        foreach ($personnages as $perso) {
            echo '<option value="'.$perso->getId().'">'.$perso->getName().'</option>';
        }
        ?>
            </select>
            <br>
            <input type="submit" value="OK">
        
        </form>

        <a href="gestionPerso.php" class="gestion">Gestionnaire des personnages, cliquez ici</a>

        </div> 

        <table>
            <tr>
                <th>Dieu</th>
                <th>PV</th>
                <th>Attaque / Force</th>
            </tr>

            <?php 
    foreach ($personnages as $perso) { ?>
            <tr>
                <td>
                    <?php echo $perso->getName() ?>
                </td>
                <td>
                    <?php echo $perso->getPv() ?>
                </td>
                <td>
                    <?php echo $perso->getAtk() ?>
                </td>
            </tr>
            <?php } ?>
        </table>
        
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
            <input type="submit" name="reinit" value="Remettre les PV à 100">
            <input type="submit" name="refresh" value="Relancer la page">
        <form>

        <?php
        if(!empty($_POST['reinit'])) {
            foreach ($personnages as $perso) {
                $perso->reinitPv();
                $manager->updatePerso($perso);
            }
        }
        ?>
    </div>
</body>

</html>