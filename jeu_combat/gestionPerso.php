<?php 
require('init.php');

$manager = new PersonnageManager($db);

if(isset($_POST['pv']) && ($_POST['pv'] >= 0)){
    $personnage = New Personnage($_POST);
    $manager->insertPersonnage($personnage);
    $perso = $manager->getLastPersonnage(1);
}

$personnages = $manager->getAllPersonnage();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Gestion des personnages</title>

    <style>
        @media screen and (max-width: 960px) {
            body {
                height: 100vh;
            }

            .main {
                position: relative;
                top: 50%;
                transform: translateY(-50%);
            }
        }
    </style>
</head>

<body>
<div class="container main">
    <form action="gestionPerso.php" method="POST">
        <fieldset>
            <legend>Ajoutez un nouveau personnage !</legend>
            <label for="name">Nom:</label>
            <input type="text" name="name">
            <br>
            <label for="pv">PV :</label>
            <input type="text" name="pv">
            <br>
            <label for="atk">ATK :</label>
            <input type="text" name="atk">
            <br>
            <input type="submit" value="Ajouter">
        </fieldset>
    </form>


    <form action="gestionPerso.php" method="POST">
        <fieldset>
            <legend>Retirez un personnage !</legend>
            <label for="id">Les personnages</label>
            <br>
            <select name="id">
                <?php 
        foreach ($personnages as $perso) {
            echo '<option value="'.$perso->getId().'">'.$perso->getName().'</option>';
        }
        ?>
            </select>
            <br>
            <input type="submit" value="Retirer">
        </fieldset>
    </form>
    <br>
    <a href="index.php"><button>Retour</button></a><br>


<?php
if (isset($_POST['pv']) && ($_POST['pv'] >= 0)){
    echo 'Vous avez ajouté '.$perso->getName(). ' qui possède '.$perso->getPv(). ' pv et a une force de ' .$perso->getAtk() .', 
    une image par défaut lui a été attribué (pour l\'instant non modifiable)';
} 
// else {
//     echo '<h1 class="erreur">Vous avez saisi un nombre de PV inférieur ou égal à 0</h1>';
// }


if (isset($_POST["id"])){
    $choix = $_POST["id"];
    $manager->deletePerso($choix);
    echo 'Vous avez supprimé '.$perso->getName();
}
?>
</div>
</body>

</html>


